################################################################################
#
# io
#
################################################################################

IO_LICENSE_FILES = NOTICE
IO_LICENSE = Apache V2.0

define IO_BUILD_CMDS
	$(TARGET_CC) $(TARGET_CFLAGS) $(TARGET_LDFLAGS) \
		package/rockchip/io/io.c -o $(@D)/io
	$(TARGET_CC) $(TARGET_CFLAGS) $(TARGET_LDFLAGS) \
		package/rockchip/io/device_state.c -o $(@D)/device_state
endef

define IO_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 755 $(@D)/io $(TARGET_DIR)/usr/bin/io
	$(INSTALL) -D -m 755 $(@D)/device_state $(TARGET_DIR)/usr/bin/device_state
endef

$(eval $(generic-package))
