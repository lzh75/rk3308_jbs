/*****************************************************************************
**
**  Name:           app_ble_wifi_introducer_main.c
**
**  Description:    Bluetooth BLE WiFi Introducer helper main application
**
**  Copyright (c) 2018, Cypress Corp., All Rights Reserved.
**  Cypress Bluetooth Core. Proprietary and confidential.
**
*****************************************************************************/
#include <stdlib.h>
#include <unistd.h>

#include "app_ble.h"
#include "bsa_avk_api.h"
#include "app_xml_param.h"
#include "app_utils.h"
#include "app_mgt.h"
#include "app_dm.h"
#include "app_manager.h"
#include "app_ble_sys_manager.h"
#include "pwmconfig.h"

/*
 * Defines
 */
extern tAPP_MGR_CB app_mgr_cb;
int ble_sys_manager_config = 0;

/*
 * Local functions
 */
static void app_ble_sys_manager_menu(void);

/*******************************************************************************
 **
 ** Function         app_ble_sys_manager_menu
 **
 ** Description      WiFi introducer helper sub menu
 **
 ** Returns          void
 **
 *******************************************************************************/
void app_ble_sys_manager_menu(void)
{
    while(ble_sys_manager_config) {
        sleep(1);
    }
}

/*******************************************************************************
 **
 ** Function         app_ble_sys_mg_mgt_callback
 **
 ** Description      This callback function is called in case of server
 **                  disconnection (e.g. server crashes)
 **
 ** Parameters
 **
 ** Returns          BOOLEAN
 **
 *******************************************************************************/
BOOLEAN app_ble_sys_mg_mgt_callback(tBSA_MGT_EVT event, tBSA_MGT_MSG *p_data)
{
    switch(event)
    {
    case BSA_MGT_STATUS_EVT:
        APP_DEBUG0("BSA_MGT_STATUS_EVT");
        if (p_data->status.enable)
        {
            APP_DEBUG0("Bluetooth restarted => re-initialize the application");
            app_ble_start();
        }
        break;

    case BSA_MGT_DISCONNECT_EVT:
        APP_DEBUG1("BSA_MGT_DISCONNECT_EVT reason:%d", p_data->disconnect.reason);
        //设置红色指示灯闪烁
        /*led_pwm_set("B",1000000000,0.0);
        led_pwm_set("G",1000000000,0.0);
        led_pwm_set("R",1000000000,0.5);
        led_pwm_set("B",1000000000,1.0);
        led_pwm_set("G",1000000000,1.0);*/
        ble_sys_manager_config = 0;
        /* Connection with the Server lost => Just exit the application */
        //exit(-1);
        break;

    default:
        break;
    }
    return FALSE;
}

/*******************************************************************************
 **
 ** Function        main
 **
 ** Description     This is the main function
 **
 ** Parameters      Program's arguments
 **
 ** Returns         status
 **
 *******************************************************************************/
int main(int argc, char **argv)
{
    int status, mode;

    APP_INFO0("SYS Manager Sensor Start");
    //led_set_color("B",1000,50);
    while(1){
        ble_sys_manager_config = 1;

        /* Initialize BLE application */
        status = app_ble_init();
        if (status < 0)
        {
            APP_ERROR0("Couldn't Initialize BLE app, exiting");
            exit(-1);
        }

        /* Open connection to BSA Server */
        app_mgt_init();
        if (app_mgt_open(NULL, app_ble_sys_mg_mgt_callback) < 0) {
            APP_ERROR0("Unable to connect to server");
            return -1;
        }

        /* Init XML state machine */
        app_xml_init();

        if (app_mgr_config(NULL)) {
            APP_ERROR0("Couldn't configure successfully, exiting");
            return -1;
        }

        /* Display FW versions */
        app_mgr_read_version();

        /* Get the current Stack mode */
        mode = app_dm_get_dual_stack_mode();
        if (mode < 0) {
            APP_ERROR0("app_dm_get_dual_stack_mode failed");
            return -1;
        } else {
            /* Save the current DualStack mode */
            app_mgr_cb.dual_stack_mode = mode;
            APP_INFO1("Current DualStack mode:%s", app_mgr_get_dual_stack_mode_desc());
        }

        /* Start BLE application */
        status = app_ble_start();
        if (status < 0) {
            APP_ERROR0("Couldn't Start BLE app, exiting");
            return -1;
        }

        app_ble_sys_manager_init();

        app_ble_sys_manager_gatt_server_init();

        /* The main BLE loop */
        app_ble_sys_manager_menu();

        /* Exit BLE mode */
        app_ble_exit();

        /* Close BSA Connection before exiting (to release resources) */
        app_mgt_close();
        APP_DEBUG0("will sleep 10 sec,then next loop!!");
        sleep(10);
    }
    APP_DEBUG0("exit app ble sys manager");
    exit(0);
}
